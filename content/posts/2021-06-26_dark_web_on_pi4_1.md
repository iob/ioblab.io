---
title: Running a dark web in pi4
subtitle: 
date: 2021-06-28
tags: ["opensuse", "raspberrypi4", "linux", "tor", "web"]
type: post
---

The steps to run a web site as an onion service is as simple as:
1. extract your distro into an SD from somewhere
2. boot into it from the raspberry
3. install tor and edit .torrc file
4. start and enable tor service

I might be tricky to find the right distro. As it turned out some distro just failed to boot properly. So you might run into try-and-fail approach.
I tried first [Fedora 34: aarch64 raw image](https://getfedora.org/en/workstation/download/) but i found that it couldnt boot due to the lack to initialize the mmc0.

# 1 Prepare SD

Many solutions for it.
I used [balena-etcher](https://www.balena.io/etcher/). As an AppImage just run without install.
After Fedora accident i got `openSUSE Leap` from http://download.opensuse.org/distribution/leap/15.3/appliances/openSUSE-Leap-15.3-ARM-X11-efi.aarch64.raw.xz

The downloaded file is a compressed image. We need to unzip it for the `ether`.

```
unxz openSUSE-Leap-15.3-ARM-X11-efi.aarch64.raw.xz
```

Insert the SD in the SD reader (make for it is not write protected) and check with `lsblk`
The ether will find and provide it as an option. Press flash and wait :)

# 2 Tor

```
sudo zypper in tor nginx
```

edit /etc/tor/torrc

under **############### This section is just for location-hidden services ###**

uncomment the following lines


```
HiddenServiceDir /var/lib/tor/hidden_service/
HiddenServicePort 80 127.0.0.1:80
```

![alt text](/images/2021-06-24_19-40_tor_enabled.png)


and run services
```
systemctl restart tor.service
systemctl enable --now tor.service 
systemctl enable --now nginx.service
```

Check *HiddenServiceDir*
```
linux:~ # ls /var/lib/tor/hidden_service/
authorized_clients  hostname  hs_ed25519_public_key  hs_ed25519_secret_key
```

Tor in this point creates the onion url for your website in the `hostname` file

![alt text](/images/2021-06-24_19-42_tor_hostname.png)


## Stay anonymous

To have a separate device to access the web through the Tor it is a big step to the online anonymity.
It takes a lot to retain it so.
You can verify that the browser is using the Tor browser visiting **https://check.torproject.org/**
If it tells you that you are not in the network go to the browser settings and add socks5 proxy on the Firefox
Default port from torrc is 9050. And check again. Double check port with torrc.

From now on the basic idea is no logins with the primary accounts. Create alternative ones if needed.
Because the Tor Browser is not runnung in arm we are kinda stuck with the normal browser that comes with the distro.
We might need to do some extra setup for privacy. Avoid to install many extra addons.
Tor Browser provides some extra tooling. You can create *new identity* or *new circuit* on demand. New identity means clear cookies and sensitive information. Loose all your running tabs and history.
You can accomplish the same with the proper settings or manual clean. New circuit means use different entry, middle and exit nodes for the websiteA. Tor Browser implies that every tab on different site uses its own unique circuit.
When you request a new circuit the website will use three other nodes. Without the tor browser, i believe that restarting the browser will give you a new circuit.

You will notice that the location that the browser sees is different every now and then. Tor changes your external ip frenquently automatically. This might cause a problem because the Google search engine, by default, will change the language to this country.
The easy solution is to not use the Google search engine. For some reason the https://www.google.com/ncr (stands for no country redirect) did not work for me. not sure why. You can also try the browser settings but i didnt try.

If you wonder if you are safe to browser the internet anonymous now, the answer is depended. I am not expert but i would be extra careful. Especially if i had use my home network to connect to the Raspberry and without change the mac address. 

# Additional OS setup

- change default root password (root:linux)

- create a normal account (optional)
```
useradd -m -d /home/iob/ -G root user1
```
- copy ssh keys from another machine to the system in Ransberry so you can connect remotely
- Configure the browser

# TL;DR

The Tor browser will not work on arm as the project is build for x86_64. I wonder if anyone has tried to compile it and run it on aarch64. Let me know if you have a link or have tried yourselves.
Having said, openSUSE leap has a rpm available even in aarch64, called torbrowser-launcher. You can run it as a normal user (Tor browser will not run with root). This will install the Tor browser
but it will refuse to launch it. The actual message was about no support on CPU or something. 

