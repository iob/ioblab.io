---
title:
subtitle: Development decisions
date: 2021-07-23
tags: ["coding"]
type: post
---

# Where your effort goes

A very common question when you have to take over a task.
A topic that can come up during refinement and triage or code reviews.

is this worth to do it?

My humble opinion is that this get conflicted too much among teams.

And personnaly i start missing the fun. The reason is that those kind of diversed opinions are unnessacery and cost in productivity as well as company's money,
because we waste time in meetings and discussions.

It would be nice if everyone had the same idea of what it should be fixed, implement next or of what should work right away.
Also depends from the team's structure and its established processes.

But because of the different views, experience, skills, interest, priorities become a hard to solve problem. Ideally you would say that
it must be an individual decision. But seems teams are confused of when to do something or not.

# Agile manifest fades out

The agile manifest gives food for discussion for long time. The effort to learn and do it right is still going on, but i have the feeling that
many fails, causing more confusion instead of solution. Agile works when you do it right. And not many can do that apparently.

# Agile and Open source communities

Many seems to agree that Agile and Open Source have many commonalities. I found two nice posts checking quickly online

- https://dreamsongs.com/IHE/IHE-28.html
- https://opensource.com/article/20/5/agile-open-source

i advice you to take a look. 

# 

Recently i read somewhere that teams they do not know what technical debt is or they deal with it wrong. In brief, too much technical debts is bad management,
but no technical debts means you are working in the wrong things.

Teams also might have some kind of meetings to estimate tasks and others not.
In the first occasion, i found rare to include the time for the code review. 
the estimation should include the code review estimation.

Another colleague brought up the idea of "implementer decides" inspired by a talk from Richard Brown with title **Distributions Are Not Democracies**[0] 

This is not exactly what i try to describe here but it helps. The idea of "implementer decides" is not also the same of what Richard is trying to emphasize.
"implementer decides" demostrates (or try to demostrate) that we do not need meetings and bring all the team onbroard when we want to implement an idea. Following
the concept of the presentation which says that democracy is not your friend when you want things done, his suggestion is who ever writes the first lines of code and
creates the prototype, that takes things forward.

When i thought about that, i found a couple of misinterpretations. You still have to estimate how much is it going to take and whether it is worth your effort.
Starting something by your own, you need somehow to find application beyond your personnal needs. Eventually you will need to invlolve others because this is what
a successful project will require. When your project becomes successful or (..) other people will start to jump in, to improve things and add features.


The estimation is hard because:

- technology is flactuated and changes quite fast every 6 months, a year or two years.
-

Decisions made based:
- Understanding the Stakeholders
- Involving Enough People
- Clear understanding of the goal and the purpose/ enough details
- taking action
- communication

- should i code this idea i have in mind
-

So it usually comes to how much time it will take and what problem is it going to solve.

We often question the necessity over the efficiency of our effort. But i want to put my personnal view of the matter.

i have been convinced that the design is a constant process which needs review and triange every now and then. You simply cant
sit back after some piece of code has found its way to the production, on our master branch or whatever. I come often across
some code that it doesnt make sense or i believe i can improve. However you do not work on a project by your own. And here are the
challenges. Many people various perspectives and opinions. Change is hard.

First, it seems that you have to convince a lot of people that this is a valuable change. 

Usually i wonder how this PR did it into the master. I wonder what the reviewers were looking for. Why the names are not following any
convension, or why there is no a design, or why did they choose to use that particular design.

# When to change and when to adapt


Some code changes are accepted and some are rejected. why, when?


[0] https://www.youtube.com/watch?v=D5YKBS-KUe8
