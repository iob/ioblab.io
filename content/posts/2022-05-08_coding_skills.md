---
title: The coding skills
subtitle: 
date: 2022-05-08
tags: ["programming", "coding"]
type: post
---


I was also obsessed with coding and tasks. Falling to sleep and thinking about all the thinks which need to be done. And i feel i am lying even in the first sentece because i still have those moments.
Like most of the people who involve coding in the daily basis, i want to become the best coder i can, learn new skills, solve problems and be ready for whatever challenge. As a new programmer, writting as much
code as you can, reading and reviewing others code is one of the good advices in this direction. I am pretty sure that many have gone through social posts, or blogs with titles like *how to become a senior developer*,
*what skills should you learn in year xxx* and so on.

Many of those posts have some good points and i am not going to write whether someone needs to do this or that. My attention is on some things that i think they do not have enough attention. Communication, problem solving and ability
to break down difficult tasks or analyse it quickly and efficient i believe are taken for granted. But i feel that reading those lists, many of us get some pressure. Without neglecting the hard work and the dedication i would like to
add a few important things from my point of view. Because the majority of the answers i found do not really give me an accurate response to balance the mentalilty that a *great programmer* needs. it also does not answer what is
really important to learn. Which most of the times is not any language or tool.

i will start accepting the argument that one important skill is to be able to learn fast. And i feel this is a good point to start because it also indicates that this is not a computer science skill. Which indirectly implies
that your skill cant be obtained only from the computing spectrum of things. An open eye and a widely view of how the world operates and what is going on in other scientific areas or not is somehow important too. Your interest, for this
reason, should not be limited only on computing. You wont be a great programmer if you know all the secrets of python language. Instead, it would be great if you know what a problem needs and pick the right language for it. Likely if you know only one language, this is the one which you will try first.

This is a small example, but i think you get the point. So when you see posts trying to tell you what you need to learn, i will say read it and ignore it. Follow your insticts. I do not understand how can someone can predict what
the future will require. Mentioning a specific tool or language or technology doesnt make sense to me. Those lists ususally reffer to things that are in hype in the current time but do not provide a ultimate solution. Our problems and our technologies are changing rapidly. And so on our needs.

Like humans too, we need to maintain our mental health. I guess all have heard about the significant of the sleep. In general, regular breaks help. Even long term breaks where you go away from daily tasks and forget to code for a while
is also ok. it doesnt matter for how long. Can be a month or two years. This time off shouldnt be considered against your productivity. I am pretty sure, many of us they need this and we keep learning things without get obsessed in coding for days and weeks. i cant see how an own-time away from coding makes you a bad programmer. Sometimes, to create something big you need the right mentality, see things clear, come with new ideas. And apparently those big projects do not come overnight. So taking your time and stay focus can brings back to you a different side of you. Find what you really need and what is attracting to you. What you really want to work on so you put in all of you.

To put an end here, i wrote my options briefly, because i think that what you need to know to become a great programmer or what you need to learn to having an IT job in the future is not on those lists in social influencers or whatever.
Is your own undertanding of the world and its problem. And what its needed for its resolution. Otherwise, instead of innovation and greatness, the most will just recycle the existing trends. Even if this is true your the majority of the developers, we can try to differentiate.
