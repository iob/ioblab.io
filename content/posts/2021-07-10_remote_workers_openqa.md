---
title: Setup remote workers on OpenQA
subtitle: When you need more jobs on your local OpenQA
date: 2021-07-10
tags: ["opensuse", "openqa", "linux"]
type: post
---


As an extension of the [[main documentation][http://open.qa/docs/#_configuring_remote_workers]] i write this describing what it took for me to
setup another machine with remote workers.

# Remote Worker side

I installed [[Leap15.3][https://get.opensuse.org/leap/#download]] with ssh enabled and i updated the packages and immediately i run

```
sudo zypper install openQA-worker
```

As per documentation we can choose the share storage. This can be rsync or nfs for example. if the later you might want to install the package as well

```
sudo zypper install nfs-client
```

The config changes i did

* on workers.ini

```
[global]
HOST = <webui>
WORKER_HOSTNAME = <workerip>

[localhost]
SHARE_DIRECTORY = /var/lib/openqa/share

[aquarius.suse.cz]
TESTPOOLSERVER = rsync://<webui>/tests
```

webui = <The hostname where the OpenQA webUI is running>
workerip = <IP address under which the worker is reachable by the web UI>

* on client.conf

```
[aquarius.suse.cz]
key = <key_from_webui>
secret = <secret_from_webui>
```

# Webui side changes

* worker.ini

In the other side where the webui is running, we can remove, delete or whatever with the `worker.ini` of that machine. it doesnt matter. We can have worker running here as well.
But we dont need to.

* nfs server

Make sure that the nfs-server is running and `/var/lib/openqa/share` is shared with `sudo showmount -e`

# TL;DR

When the time comes to test your workers, first you should be able to check the `/admin/workers` and find them as online.
Obvious. And then lets run a test. And you see this :)

```
[2021-07-07T09:50:48.0523 CEST] [info] +++ setup notes +++
[2021-07-07T09:50:48.0524 CEST] [info] Running on dhcp76:1 (Linux 5.3.18-59.5-default #1 SMP Wed Jun 2 08:21:36 UTC 2021 (eaf040d) x86_64)
[2021-07-07T09:50:48.0543 CEST] [debug] +++ worker notes +++
[2021-07-07T09:50:48.781 CEST] [debug] Current version is 4.6.1606482144.ab6cdfe2 [interface v20]
[2021-07-07T09:50:48.796 CEST] [debug] git hash in /var/lib/openqa/share/tests/sle: f6b74f0d1ed56a8456b8856ccaadb4af9e96a33e
String found where operator expected at /var/lib/openqa/share/tests/sle/lib/mm_network.pm line 19, near "enter_cmd "echo '$hostname' > /etc/HOSTNAME""
	(Do you need to predeclare enter_cmd?)
String found where operator expected at /var/lib/openqa/share/tests/sle/lib/mm_network.pm line 20, near "enter_cmd "echo '$hostname' > /etc/hostname""
	(Do you need to predeclare enter_cmd?)
String found where operator expected at /var/lib/openqa/share/tests/sle/lib/mm_network.pm line 21, near "enter_cmd "hostname '$hostname'""
	(Do you need to predeclare enter_cmd?)
String found where operator expected at /var/lib/openqa/share/tests/sle/lib/mm_network.pm line 24, near "enter_cmd "hostnamectl set-hostname '$hostname'""
	(Do you need to predeclare enter_cmd?)
String found where operator expected at /var/lib/openqa/share/tests/sle/lib/mm_network.pm line 70, near "enter_cmd 'done'"
	(Do you need to predeclare enter_cmd?)
syntax error at /var/lib/openqa/share/tests/sle/lib/mm_network.pm line 19, near "enter_cmd "echo '$hostname' > /etc/HOSTNAME""
syntax error at /var/lib/openqa/share/tests/sle/lib/mm_network.pm line 24, near "enter_cmd "hostnamectl set-hostname '$hostname'""
syntax error at /var/lib/openqa/share/tests/sle/lib/mm_network.pm line 70, near "enter_cmd 'done'"
Compilation failed in require at /var/lib/openqa/share/tests/sle/lib/utils.pm line 25.
BEGIN failed--compilation aborted at /var/lib/openqa/share/tests/sle/lib/utils.pm line 25.
Compilation failed in require at /var/lib/openqa/share/tests/sle/lib/registration.pm line 23.
BEGIN failed--compilation aborted at /var/lib/openqa/share/tests/sle/lib/registration.pm line 23.
Compilation failed in require at /var/lib/openqa/share/tests/sle/products/sle/main.pm line 16.
BEGIN failed--compilation aborted at /var/lib/openqa/share/tests/sle/products/sle/main.pm line 16.
Compilation failed in require at /usr/lib/os-autoinst/OpenQA/Isotovideo/Utils.pm line 210.

31671: EXIT 1
[2021-07-07T09:50:48.0818 CEST] [info] Isotovideo exit status: 1
[2021-07-07T09:50:48.0873 CEST] [info] +++ worker notes +++
[2021-07-07T09:50:48.0874 CEST] [info] End time: 2021-07-07 07:50:48
[2021-07-07T09:50:48.0874 CEST] [info] Result: died
[2021-07-07T09:50:48.0881 CEST] [info] Uploading vars.json
[2021-07-07T09:50:48.0972 CEST] [info] Uploading autoinst-log.txt
```

That's a weird error. And in short, the solution is simple. Make sure that you run the same `os-autoinst` version in both ends.
My error above comes from the fact that Leap had older version than Tumbleweed. In this case, to try just to update the package
with the main repos will not work. You need to add [[devel_openQA repo][https://download.opensuse.org/repositories/devel:/openQA/openSUSE_Leap_15.3/x86_64/]]

```
zypper addrepo https://download.opensuse.org/repositories/devel:openQA/openSUSE_Leap_15.3/devel:openQA.repo
zypper ref
zypper up os-autoinst
```

And try again... :)

For some reason, there are still some issues which i havent investigated. One of them is the communication with the websocket. When a
job is running, the webui does not update the status. However test and the live feeding looks just fine.
