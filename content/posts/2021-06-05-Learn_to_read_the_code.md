---
title: Did you read the code?
subtitle: Code review ?
date: 2021-05-30
tags: ["programming", "skills"]
type: post
---

### We dont learn to read code

If you write code or even you write some scripts for personnal matters, i bet you have read code of other here and there.
I hope no ones is forced to read code that he doesnt want but it is unlikely to not.
And my feeling is that this process is a bit overseen. You learn how to code, online or in University or wherever, but you do not learn how to read.
And it seems important to me nowdays. The code base is getting more and more complicated, design patterns and different languages.

And then it comes something that it is called code review and you have to do it in one point or the other.
What a painful topic. Code review can cause so many conflicts and arguments.
Doesnt matter whether you have gone through all of those online guides of how to do it right, what to not do, how much time to spend on reviewing etc.
Did any of them work? I am not quite sure. Why? First of all the theory is just different from the reality.
So many different players and roles. So many different personalities, with different background and different priorities and expertises. Everyone with a different opinion.

Really.. And we do not learn to do that too. We try to figure it out. Maybe for the biggest part of our carreer.


### Read Code is as important as the coding itself

Whatever the code review process your team has or doesnt, the exercise itself it is important for you if you care about what you are doing. I bet you love what you do and in fact you try to be good. 
Do you need to comment every single line of code you find wrong. No. Just read. See what others are up to. You may learn something new. Neverthereless you will have a better view of the project. you will be able to avoid mistakes on your own commit (no promise that it will not get comments even so!!), and comply with the techniques and the old implementations. When the time comes for a small or larger refactoring, you might know what you need to change and where. Extra bonus that you will be able to help others if you are asked or if you want. But what the benefits if you do not take some time to read some code of your project that you work on, or the code that others contribute. I cant think any if you care and if you love what you do. I would say even if you dont, it is better, in my opinion, to take some time out of your working hours to do this than merely try to finish other tasks.

I have met some that they avoid to do code review because they are not so good coders or they are in a new position, new project. bullshits. In fact you should do reviews even in other open source projects. Or to be accurate, read others' projects code. Go to github for instance and start reading. Question whatever you want, like what the coder chooses this way than the other, how would it could be better, what is it missing, any bad practices etc. There are a few things i have learnt from reading others code. For new comers, it might be an opportunity to contribute in open source. 

When it comes to your own code which get reviewed by others, keep notes. Another case that i found that reading makes different.
Even the smallest review comment can make a difference or be important. Tiimes after times i read a comment for first time and i overlook what else can this mean or other implications can be underneath. In first sight, might, make no difference or there is no problem. Usually those comments take me some time to figure, with one debate after another. Sometime it can be nothing more on this and the reviewer will have to figure it out and help you end the back and forth discussion. Be patient.

Nevertheless, people have different view of how things should look like. And there are times that you can get along. Personally i believe that this is ok. Diversity is productive. But i wish some of those comments to fall in the space of individual responsibility. You want to have a commit as concrete and complete as possible. So why not even than ask for a change, next time just pull and create a new commit. Breaking down the task and maintain a good git history is significant. But not everyone cares. And i think this is a selfish and arogant.

Convenience and design can collide. Deadlines and efficience might conflict as well. Thus learning to read and understand the developer behind the code can be your friend. 


### How should you really do it

That is a complicate riddle to work out. you can as many post online here and there. Sure, there are many good points but no of them really help in essense in my opinion. At the end it comes to the people you work with.
It should be a group responsibility and team work. With no some kind of communication, in sense of common understanding and ground rules, you will always get some hard times. And even if those sounds like easy to have, in fact the reality seems different. I am told recently, when i was complaining about the review process in my team that things cant change and as an open source project there are no much we can change. Do not fall for this shit. QA is about to fix those processes that cause conflicts and workflows. And there are many good examples out there. But you have to choose your battles and apparently you can not change everything.


### TL;DR

Reading code should be fun. For me, do not need to comment everything you do not like. keep notes. learn from what the others thing. Know your base code and just take responsibility of your own patches.
Most of the people are selfish and egocentic. And i am fighting with it with my self. this sense of ownership and this is better than the other. However i have come to the agreement with myself that without the fun part it isnt worth it. If you have conflicts and disagreements, they belongs out of the code review process. Best solution, maybe, is to automate as much as you can, such as to avoid as much human interaction.
