---
title: "My openqa dev env"
subtitle: "Intro"
tags: ["opensuse", "openqa"]
date: 2023-03-04
type: post
---

* Second OpenQA Instance

This is an clumsy approach to run an openQA development environment besides the regular one. And yes it seems to
be possible, with some caveats i guess. I am guessing because it is not fully functional and i wonder whether it
is conflicted by other services somehow.

With this intro, i warn that this is not a complete guide and i take it as i will have to come back on it some other time.

Briefly, the steps comes from [openQA-helper](https://github.com/Martchus/openQA-helper). Martchus is one of the main developers
and he maintains this repo. For my needs i just created a few scripts to get it up and running once his scripts had finished.

The end structure look like that under `openqa-devel` directory

```
build  config  openqa  repos  sql-dumps  sql-dumps-on-wotan
```

where:
config is the location where the openqa.ini, client.conf and workers.conf live
openqa is the /var/lib/openqa directory with the logs and tests
repos locates os-autoinst and openQA repos

* Run dev environment

This is a script i use based on the instructions from Martchus.
`OPENQA_KEY` and `OPENQA_SECRET` have to be added once you access the openQA UI the first time and create those keys

```
export OPENQA_BASEDIR=/home/iob/opt/openqa-devel
export OPENQA_CONFIG=$OPENQA_BASEDIR/config
export OPENQA_LIBPATH=$OPENQA_BASEDIR/repos/openQA/lib # for foursixnine's way to let os-autoinst find openQA libs
export OPENQA_LOCAL_CODE=$OPENQA_BASEDIR/repos/openQA
export OPENQA_CGROUP_SLICE=systemd/openqa/$USER
#export OPENQA_KEY=set_later
#export OPENQA_SECRET=set_later
export OPENQA_SCHEDULER_WAKEUP_ON_REQUEST=1
export OPENQA_SCHEDULER_SCHEDULE_TICK_MS=1000
export OPENQA_BASE_PORT=7900
export PATH="$PATH:/usr/lib64/chromium:$OPENQA_BASEDIR/repos/openQA-helper/scripts"
alias openqa-cd='source openqa-cd' # allows to type openqa-cd to cd into the openQA repository
echo "openqa variables exported"
# sudo systemctl stop openqa-webui
# sudo systemctl stop openqa-websockets
# sudo systemctl stop openqa-scheduler
# sudo systemctl stop openqa-livehandler
# sudo systemctl stop openqa-gru
# echo "openqa services stopped"
pushd /home/iob/opt/openqa-devel/repos/openQA-helper
screen -S wu -d -m openqa-start wu
screen -S ws -d -m openqa-start ws
screen -S sc -d -m openqa-start sc
screen -S lv -d -m openqa-start lv
screen -S gru -d -m openqa-start gru run
screen -S wo -d -m openqa-start wo
echo "openqa dev is up"
```

The magit with `screen` is that you do not have to open separate terminals to run each service. Instead you can jump into and out them
whenever it is needed.

* Where is my test repo

```
~/opt/openqa-devel/openqa/share/tests/opensuse/tests
```
it also includes `sle`. For other distros we can make a symlink

* Stop dev environment

And this just undo all the steps from the first script

```
unset OPENQA_BASEDIR
unset OPENQA_CONFIG
unset OPENQA_LIBPATH
unset OPENQA_LOCAL_CODE
unset OPENQA_CGROUP_SLICE
unset OPENQA_KEY
unset OPENQA_SECRET
unset OPENQA_SCHEDULER_WAKEUP_ON_REQUEST
unset OPENQA_SCHEDULER_SCHEDULE_TICK_MS
unset OPENQA_BASE_PORT
unset OPENQA_KEY
unset OPENQA_SECRET
export PATH="/home/iob/bin:/usr/local/bin:/usr/bin:/bin"
unalias openqa-cd
# killall screen sessions

screen -S wo -X quit
screen -S gru -X quit
screen -S lv -X quit
screen -S wu -X quit
screen -S ws -X quit
screen -S sc -X quit

# restart openqa services
# sudo systemctl start openqa-webui
# sudo systemctl start openqa-websockets
# sudo systemctl start openqa-scheduler
# sudo systemctl start openqa-livehandler
# sudo systemctl start openqa-gru
```
