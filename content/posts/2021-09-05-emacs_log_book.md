---
title: Setup logbook in emacs
subtitle: Improving coding 
date: 2021-09-05
tags: ["emacs", "linux", "programming", "org-mode"]
type: post
---

# Logbook for programming

Recently i finish the `The cuckoo's egg` which a guy with astronomic background found a hacker in his computer
and he started watching him messing around and take notes of what he was doing. That was his logbook.

This makes me search what logbook used for in general. And then i searched for logbook from programmers.
The main reference was [a block post](https://routley.io/posts/logbook/) which the only thing that you can find is
the idea of using logbook for programming and a bash script to create a dated markup file.

Of course this was not enough as context to introduce how a logbook can be used.

in contrast of the above post, i use emacs and org-mode.

i am not expert so i took me some effort.

To reach my goal on emacs/org i have to get familiar with a few things and leverage the power of org-mode.
One of those feature is `org-capture` and particular the `org-capture-templates`.

pressing Ctrl+h f(C-h f) and search for org-capture you can find a few info to start. i quote the section
that more relevant for us here

```
This will let you select a template from ‘org-capture-templates’, and
then file the newly captured information.  The text is immediately
inserted at the target location, and an indirect buffer is shown where
you can edit it.  Pressing ‘C-c C-c’ brings you back to the previous
state of Emacs, so that you can continue your work.
```

That means that we can create a shortcut which every time will open a new buffer in the location we want
and it insert whatever we have instructed to do.
You can *design* your logbook as you like to keep what ever info you want.
The basic idea is that you have a location where you will store all the files.Each file will have the format of `$(date '+%Y-%m-%d').org`

The simpliest logbook could have
one line with the timestamp and your comment. Every time we press the shortcut will open the same org file and we will add a new entry.

Let do that

```;;~/.emacs
(use-package org
  :config
  (setq org-agenda-start-with_log-mode t)
  (setq org-log-done 'time)
  (setq org-log-into-drawer t)
  (setq org-agenda-files '("/home/iob/.tasks.org"))

  (defun iob/org-file-by-date ()
  "Create an Org file with current time as name."
  (find-file (format-time-string "~/logbook/%Y-%m-%d.org")))
  
  (setq org-capture-templates
    '(("b" "logbook" plain
      (function iob/org-file-by-date)
      "** %<%H:%M:%S %p %Z> - %?\n" :clock-in t :clock-resume t :empty-lines 1)
     ;; other templates here
     ("t" "Tasks")
       ("tt" "Task" entry (file+olp "/home/iob/.tasks.org" "Tasks")
        "* TODO %?\n  %U\n  %a\n  %i" :empty-lines 1))))
```

Save and relaunch emacs. Now press `C-c c`. A menu appears. With `b` the new org file with the current date
will be created and it will be waiting for some entry.
for instance something like


![org-capture opens a buffer and a preview window](/images/emacs_log_book/logbook_hugo.png)


with a cursor blinking at the end. Enter something and press C-c C-c to save and exit.

So the first parameter is the shortcut, then the text to describe the shortcut and then the format of the file+path_to_file.
the last parameter is what will be inserted every time you open the file with some extra parameters which org-mode
can understand[0]. The string part can be modified in your needs. So lets say you want everytime you open the file to add the timestamp, a tag and a note.
And you want to locate the cursor after tag, then you can have something like

```
"** %<%H:%M:%S> \nTAG: %?\nNOTES: \n" :clock-in t :clock-resume t :empty-lines 1
```


To show the basic construction think the simplier version

```
("b" "logbook" plain ~/mylogbook.org "* %?")
```

However we need a new org file each day and the trick is happening with `iob/org-file-by-date` function.

# logbook methodologies

Many programmers use different methodologies or techniques to implement a software or solve a problem.
And programming is kinda repetitive task. You might know that you do not need everything and it is ok
to go to the Stackoverflow to find what you need.

However programming is art and it makes easier if you get organized.

With organized i mean:

- Document what and why you do something.
- Document how did you solve a specific problem and its specularities.
- Schedule your task and your day

A logbook is intented to make exactly that with a few benefits.

- When you debug you have a clear idea of the approach. Make it easier to review and see what works and what not.
- When you think about the problem and describe it beforehands the coding can be straight forward.
- Often you forget what you have done or why. no need to google it all the time again and again
- Writing your thinking process can improve your coding skills. New coders can see what they did wrong and how they can impove
- When everything is logged you can see how you went from one idea to another. That possibly will save you sometime in a logn run.
- Improve your commit messages.
- Find how much do you spend for each task (and improve it)

Having listed all those, i think it is obviously that the use of logbook is depended from your goals.
You can organize your notes in a way that fits your objectives and your workflow.

For instance:
- You can log at the end of the day what you have done, what you learn or what problems you solved and what you want to work tomorrow
- If you use some kind of pomodoro technique you can log at the end of the session what you did and what you think you should do/try in the next session.
- Use it as a rubber duck debugging.
- make it a diary

Maybe you are looking for something more specific. But i can tell only how i use it. and this is a little bit of all
the above. I still experiment with the best format and how to organize everything so i can find them later. i will
update once i have a solid approach. Keep in mind that org-mode gives a lot of possibilities and nevertheless, the organization of the workflow doesnt limit only on logbook.


# Something extra for your org-mode configuration

If you try org-mode for first time you might find that some shortcut are not working out of the box.
The following are common ones which needs to be defined manually.

```
;;~/.emacs
(global-set-key (kbd "C-c l") 'org-store-link)
(global-set-key (kbd "C-c a") 'org-agenda)
(global-set-key (kbd "C-c c") 'org-capture)
```

---


[0] from the `org-capture-templates` documentation
>
> empty-lines
> : Set this to the number of lines that should be inserted
>                     before and after the new item.  Default 0, only common
>                     other value is 1.
>
> clock-in
> : Start the clock in this item.
>
> clock-resume
> : Start the interrupted clock when finishing the capture.
>                     Note that :clock-keep has precedence over :clock-resume.
>                     When setting both to t, the current clock will run and
>                     the previous one will not be resumed.

Emacs key binds:
> C-c
> : Ctrl+c
