---
title: Build a kernel out of OBS
subtitle: The classic way
date: 2022-02-17
tags: ["kernel", "linux"]
type: post
---

The magic of OBS performs sometimes, hides some important steps. For instance the signing of the modules. Trying to build the kernel outside of the build system, you will fail without the required keys which are used for the signing.  My understanding is that OBS will create the keys somewhere in the Build system. The keys are stored in *.kernel_signing_key.pem* and if we examine the kconfig will find the following entry

`CONFIG_MODULE_SIG_KEY=/home/abuild/rpmbuild/SOURCES/.kernel_signing_key.pem`

Notice that the corresponding value on TW is the default path of the *CONFIG_MODULE_SIG_KEY*

`CONFIG_MODULE_SIG_KEY="certs/signing_key.pem"`

So it is obvious that /CONFIG_MODULE_SIG_KEY/ file does not carry on, which means that we can find suck path or file in the system. 

Also in my understanding, /CONFIG_SYSTEM_TRUSTED_KEYRING/ is used to verify the signing of the keys. So if we do not go with the [creation of our own keys](https://www.kernel.org/doc/html/v4.10/admin-guide/module-signing.html), it might be usuful to disable it. 

## few words of the kernel subprojects

The main branch can be found https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/. Notice the `git/torvalds`? This is a branch that it is controlled by the Linus Torvalds himself. But the git.kernel.org has many subprojects where the actual development is happening. When the development is done, the maintainer of this subproject will create a merge request to the Torvalds' tree. Linus' job is to check every day those request and accept or reject them.

A such a subproject is the rdma (the one i had to use in my case). We can fetch it from https://git.kernel.org/pub/scm/linux/kernel/git/rdma/rdma.git/. The tree of rdma is identical of the main branch and we can build this particular one, in case to test some specific feature for rdma.

## Building kernel from a subproject

```
git clone --depth 1 --single-branch --branch for-next https://git.kernel.org/pub/scm/linux/kernel/git/rdma/rdma.git linux-rdma
cd linux-rdma
```

Now we need to get the config of the old kernel, the one currently used and do some changes
```
zcat /proc/config.gz > .config
sed -i 's/CONFIG_MODULE_SIG_KEY=.*/CONFIG_MODULE_SIG_KEY=""/' .config
sed -i 's/CONFIG_SYSTEM_TRUSTED_KEYRING=.*/CONFIG_SYSTEM_TRUSTED_KEYRING=n/' .config
```
Both SLE and TW failed also due to the /CONFIG_DEBUG_INFO_BTF/ so i disabled that as well
```
sed -i 's/CONFIG_DEBUG_INFO_BTF=y/# CONFIG_DEBUG_INFO_BTF=y/' .config
```

Now it should be ready to run the tools to set the default keys and compile it. Because the repo is not the main one we can enable the /allow_unsupported_modules/ to install its modules too.

```
# from the root where *.config* is located
make olddefconfig
make -j `nproc`
sed -i 's/allow_unsupported_modules 0/allow_unsupported_modules 1/g' /lib/modprobe.d/10-unsupported-modules.conf
make modules_install
make install
```

The _olddefconfig_ will take all the default values of the old kernel configuration and set those to the *.config* for the new kernel. The difference with _defconfig_ is that if the value is not different the compiler will ask which value to apply, where the _olddefconfig_ will get the default value of the old and will apply it without question it.

The last step should build the initrd and create the vmlinuz which we should copy to the /boot. And update the bootloader so it adds an entry of our new kernel
```
mkinitrd /boot/initrd-$(make kernelrelease) $(make kernelrelease)
cp /boot/vmlinuz /boot/vmlinuz-$(make kernelrelease)
update-bootloader
```

Check the existence of `vmlinuz-*` and `initrd-*` and grub files and reboot to test.
/vmlinuz/ contains the kernel and will need /initrd/ which is used as the root fs before the final fs is mounted.
