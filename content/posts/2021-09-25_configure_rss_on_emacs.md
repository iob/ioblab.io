---
title: Configure rss feeds on emacs for Newsticker
subtitle: Newsletters in emacs
date: 2021-09-25
tags: ["emacs", "linux"]
type: post
---

# A good reason

It is hard for me to follow news here and there. So instead of open every single site i want to check on reddit, on google news or on Linux Insider i turn to the Newsticker.
I found it quite efficient to start off. Although i had to find all the links which i am interesting in manually and add them one by one. And mainly the post it is about this.
Using https://rss.app Bad news you need a google or twitter account.

# Create customized RSS feed

We have to go to https://rss.app/new-rss-feed which provides us with a RSS feed generator

![RSS generator](/images/newsticker/rss_generator.png)

Now lets say you want to get the world news from google.
In such case you can visit the https://news.google.com/ select the topic, that *World* in our case and copy the url.
Go back to the rss.app/new-rss-feed and select Google. Redirects to https://rss.app/new-rss-feed/google-news-rss-feed where you can
paste the copied address. That will generate a url like *https://rss.app/feeds/lpT1vyfmcOC7SI9L.xml*.

From here we can open *newsticker* on emacs and press **a** to add the rss link to the list. Give a name and press *g* or *G* to fetch the news.

# Finding other rss feeds

I have added a few others which i know serve the open source community with interesting articles. I found them [here](https://blog.feedspot.com/linux_rss_feeds/)

Using the rss.app we can easily get a rss link for reddit, twitter, cnn and others.

But this is one way. Propably eveyone recognise the rss symbol. That s the obvious way.

Then there are some tricks. like check the source code in the web page, and /feeds/ at the end of a blog and use some browser's plugins.
In you are looking for some particular platform, you may want to check the help links. for instance

- medium provides https://help.medium.com/hc/en-us/articles/214874118-Using-RSS-feeds-of-profiles-publications-and-topics
- wikipedia you can search for *rss* in help or use [this main feed](http://en.wikipedia.org/w/index.php?title=Special:Recentchanges&feed=rss)
- dev.to using https://dev.to/feed
- ieee.org has also some info [here](https://ieeexplore.ieee.org/Xplorehelp/ieee-xplore-training/user-tips)

## resources
[1] https://www.howtogeek.com/320264/how-to-get-an-rss-feed-for-any-subreddit/
[2] https://gist.github.com/thefranke/63853a6f8c499dc97bc17838f6cedcc2
[3] https://www.gnu.org/software/emacs/manual/html_mono/newsticker.html#Reading-News
[4] https://www.streamingmediablog.com/2007/08/list-of-rss-fee.html
[5] https://www.reddit.com/r/pathogendavid/comments/tv8m9/pathogendavids_guide_to_rss_and_reddit/
